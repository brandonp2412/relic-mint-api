const express = require("express");
const asyncHandler = require("express-async-handler");
const passport = require("passport");
const marketCtrl = require("../controllers/market.controller");

const router = express.Router();
module.exports = router;

router.use(passport.authenticate("jwt", { session: false }));
router.patch("/", asyncHandler(setRelicValues));

async function setRelicValues(req, res) {
  const response = await marketCtrl.valueRelics(req.user.email);
  res.json(response);
}
