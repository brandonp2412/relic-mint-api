const mongoose = require("mongoose");

const RelicSchema = new mongoose.Schema(
  {
    era: {
      type: String,
      required: true,
      match: [/Lith|Meso|Neo|Axi/, "Please enter a valid Era."],
      maxLength: 4
    },
    name: {
      type: String,
      required: true,
      match: [/[A-Z]\d\d?/, "Please enter a valid name."],
      maxLength: 3
    },
    rareDrop: {
      type: String,
      required: true,
      maxLength: 255
    },
    minSell: {
      type: Number,
      default: 0
    },
    maxBuy: {
      type: Number,
      default: 0
    },
    users: {
      type: Array
    },
    valuedAt: {
      type: Date
    }
  },
  {
    versionKey: false
  }
);
RelicSchema.index({ era: 1, name: 1 }, { unique: true });
RelicSchema.index({ era: "text", name: "text", rareDrop: "text" });

module.exports = mongoose.model("Relic", RelicSchema);
