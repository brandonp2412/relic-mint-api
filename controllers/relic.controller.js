const Joi = require("joi");
const Relic = require("../models/relic.model");
const mongo = require("mongoose").mongo;

const relicSchema = Joi.object({
  _id: Joi.string(),
  era: Joi.string().required(),
  name: Joi.string().required(),
  rareDrop: Joi.string().required(),
  minSell: Joi.number(),
  maxBuy: Joi.number()
});

module.exports = {
  insert,
  query,
  querySearch,
  remove,
  update,
  find,
  queryForUser,
  popUser,
  pushUser
};

async function insert(relic) {
  relic = await Joi.validate(relic, relicSchema);
  return await new Relic(relic).save();
}

async function query(sort, order, page) {
  relics = Relic.find();
  relics = await paginate(relics, sort, order, page).exec();
  const total = await Relic.countDocuments();
  return { relics: relics, total: total };
}

async function querySearch(sort, order, page, search) {
  let relics = Relic.find({ $text: { $search: search } });
  relics = await paginate(relics, sort, order, page).exec();
  const total = await Relic.find({
    $text: { $search: search }
  }).countDocuments();
  return { relics: relics, total: total };
}

async function pushUser(email, ids) {
  return await Relic.updateMany(
    { _id: { $in: ids } },
    { $addToSet: { users: email } }
  ).exec();
}

async function popUser(email, id) {
  return await Relic.updateOne(
    { _id: id },
    { $pullAll: { users: [email] } }
  ).exec();
}

async function queryForUser(email, sort, order, page, search) {
  let total = search
    ? Relic.find({ $and: [{ $text: { $search: search } }, { users: email }] })
    : Relic.find({ users: email });
  total = await total.countDocuments().exec();
  let relics = search
    ? Relic.find({ $and: [{ $text: { $search: search } }, { users: email }] })
    : Relic.find({ users: email });
  relics = await paginate(relics, sort, order, page).exec();
  return { relics: relics, total: total };
}

function paginate(relics, sort, order, page) {
  return relics
    .sort([[sort, order]])
    .limit(10)
    .skip(10 * page)
    .select("-users");
}

async function remove(id) {
  return await Relic.findOne({ _id: id })
    .remove()
    .exec();
}

async function update(relic) {
  relic = await Joi.validate(relic, relicSchema);
  return await Relic.update({ _id: relic._id }, relic).exec();
}

async function find(id) {
  return await Relic.findOne({ _id: id })
    .select("-users")
    .exec();
}
