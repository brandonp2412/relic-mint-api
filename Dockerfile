FROM node:8

WORKDIR /usr/src/app
ADD . .

RUN npm install 

EXPOSE 4040

CMD ["npm", "start"]