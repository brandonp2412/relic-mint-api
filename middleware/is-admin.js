const httpError = require("http-errors");

const isAdmin = function(req) {
  if (req.user && req.user.roles.indexOf("admin") > -1) return;
  throw new httpError(401);
};

module.exports = isAdmin;
