const request = require("request-promise-native");
const Relic = require("../models/relic.model");

const resourceUrl = "https://api.warframe.market/v1/items/";

module.exports = {
  valueRelics
};

async function valueRelics(email) {
  let relics = await Relic.find({ users: email });
  let _nModified = 0;
  for (const relic of relics) {
    const now = new Date();
    const updatedDiff = now - relic.valuedAt;
    if (updatedDiff / 1000 / 60 < 5) return;
    const rareDrop = relic.rareDrop
      .toLowerCase()
      .replace(/\s/g, "_")
      .replace("&", "and");
    const orders = await getOrders(rareDrop);
    valueRelic(relic, orders);
    _nModified++;
  }
  return { n: relics.length, nModified: _nModified, ok: 1 };
}

async function getOrders(rareDrop) {
  const response = await request.get(`${resourceUrl}/${rareDrop}/orders`);
  await sleep(1000);
  let orders;
  try {
    orders = JSON.parse(response).payload.orders;
  } catch (error) {
    console.error(`Failed to parse JSON response from rareDrop "${rareDrop}"`);
    console.error(error);
  }
  return orders;
}

function valueRelic(relic, orders) {
  const minSell = orders
    .filter(order => order.order_type === "sell")
    .filter(order => order.user.status === "ingame")
    .map(order => order.platinum)
    .sort((a, b) => a - b)[0];
  const maxBuy = orders
    .filter(order => order.order_type === "buy")
    .filter(order => order.user.status === "ingame")
    .map(order => order.platinum)
    .sort((a, b) => b - a)[0];
  relic.minSell = minSell;
  relic.maxBuy = maxBuy;
  relic.valuedAt = new Date();
  relic.save();
}

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}
