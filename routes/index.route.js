const express = require("express");
const userRoutes = require("./user.route");
const authRoutes = require("./auth.route");
const relicRoutes = require("./relic.route");
const marketRoutes = require("./market.route");

const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get("/health-check", (_req, res) => res.send("OK"));

router.use("/auth", authRoutes);
router.use("/user", userRoutes);
router.use("/relic", relicRoutes);
router.use("/market", marketRoutes);

module.exports = router;
