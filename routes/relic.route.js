const express = require("express");
const passport = require("passport");
const asyncHandler = require("express-async-handler");
const relicCtrl = require("../controllers/relic.controller");
const isAdmin = require("../middleware/is-admin");

const router = express.Router();
module.exports = router;

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  asyncHandler(create)
);
router.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  asyncHandler(remove)
);
router.put(
  "/",
  passport.authenticate("jwt", { session: false }),
  asyncHandler(update)
);
router.patch(
  "/push-me",
  passport.authenticate("jwt", { session: false }),
  asyncHandler(pushUser)
);
router.patch(
  "/:id/pop-me",
  passport.authenticate("jwt", { session: false }),
  asyncHandler(popUser)
);
router.get(
  "/my",
  passport.authenticate("jwt", { session: false }),
  asyncHandler(queryForUser)
);
router.route("/").get(asyncHandler(query));
router.route("/:id").get(asyncHandler(find));

async function queryForUser(req, res) {
  const search = req.query.search;
  const query = await relicCtrl.queryForUser(
    req.user.email,
    req.query.sort,
    req.query.order,
    req.query.page,
    search && search !== "undefined" ? search : false
  );
  res.json({ total: query.total, entities: query.relics });
}

async function popUser(req, res) {
  res.json(await relicCtrl.popUser(req.user.email, req.params.id));
}

async function pushUser(req, res) {
  res.json(await relicCtrl.pushUser(req.user.email, req.body));
}

async function create(req, res) {
  isAdmin(req);
  const relic = await relicCtrl.insert(req.body);
  res.json(relic);
}

async function query(req, res) {
  const sort = req.query.sort;
  const order = req.query.order;
  const page = req.query.page;
  const search = req.query.search;
  const query =
    search && search !== "undefined"
      ? await relicCtrl.querySearch(sort, order, page, search)
      : await relicCtrl.query(sort, order, page);
  res.json({ total: query.total, entities: query.relics });
}

async function remove(req, res) {
  isAdmin(req);
  res.json(await relicCtrl.remove(req.params.id));
}

async function update(req, res) {
  isAdmin(req);
  res.json(await relicCtrl.update(req.body));
}

async function find(req, res) {
  res.json(await relicCtrl.find(req.params.id));
}
